# openraven-terraform

A terraform module for deploying the discovery role for Open Raven.

## Usage

```terraform
module "openraven-discovery" {
  source               = "git::https://gitlab.com/openraven/open/openraven-terraform.git"
  discovery_account_id = "<Account ID being deployed to>"
  external_id          = "<External ID from Open Raven 'Add new AWS Account' screen>"
  openraven_org_id     = "<Open Raven Org ID from Open Raven 'Add new AWS Account' screen>"
}
```

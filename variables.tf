variable "discovery_account_id" {
  type        = string
  description = "The account Id of the AWS account this is being deployed into"
}

variable "openraven_org_id" {
  type        = string
  description = "Org Id from Open Raven. This will be provided in the Add Account Wizard"
}

variable "external_id" {
  type        = string
  description = "External Id used as part of the cross-account trust policy. This will be provided in the Add Account Wizard"
}

variable "openraven_aws_account_id" {
  type        = string
  default     = "230888199284"
  description = "This is the AWS Account that your Open Raven instance is running in. DO NOT CHANGE THIS WITHOUT EXPLICIT INSTRUCTIONS FROM OPEN RAVEN SUPPORT"
}

variable "enable_rds_snapshot_scanning" {
  type        = bool
  default     = true
  description = "This enables or disables the policy that is needed to perform RDS snapshot scanning"
}

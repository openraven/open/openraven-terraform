terraform {
  required_version = "~> 1.0"
  required_providers {
    aws = {
      # https://registry.terraform.io/providers/hashicorp/aws/latest
      version = "~> 5.0"
      source  = "hashicorp/aws"
    }
  }
}

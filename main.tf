resource "aws_iam_role" "openraven-discovery-role" {
  # ORVNDiscoveryRole
  name        = "openraven-cross-account-${var.openraven_org_id}"
  description = "Allow Open Raven to monitor and discover resources."
  assume_role_policy = jsonencode({
    Version = "2008-10-17"
    Statement = [
      {
        Effect = "Allow"
        Principal = {
          AWS = "arn:aws:iam::${var.openraven_aws_account_id}:role/orvn-${var.openraven_org_id}-cross-account"
        }
        Action = "sts:AssumeRole"
        Condition = {
          StringEquals = {
            "sts:ExternalId" = var.external_id
          }
        }
      },
      {
        Effect = "Allow"
        Principal = {
          Service = "lambda.amazonaws.com"
        }
        Action = "sts:AssumeRole"
      }
    ]
  })
}

resource "aws_iam_policy" "openraven-lambda-setup" {
  # ORVNDiscoveryPolicy
  name = "openraven-lambda-setup-${var.openraven_org_id}"
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "lambda:CreateFunction",
          "lambda:InvokeFunction",
          "lambda:GetFunction",
          "lambda:DeleteFunction",
          "lambda:TagResource",
        ]
        Resource = [
          "arn:aws:lambda:*:${var.discovery_account_id}:function:dmap-*"
        ]
        Effect = "Allow"
      },
      {
        Action = "iam:PassRole"
        Resource = [
          aws_iam_role.openraven-discovery-role.arn,
        ]
        Effect = "Allow"
      },
      {
        Effect = "Deny",
        Action = [
          "logs:CreateLogGroup",
          "logs:CreateLogStream",
          "logs:PutLogEvents",
        ]
        NotResource = [
          "arn:aws:logs:*:*:*debug*"
        ]
      }
    ]
  })
}

resource "aws_iam_policy" "openraven-backup-enablement" {
  # ORVNBackupEnablementPolicy
  name = "openraven-backup-enablement-${var.openraven_org_id}"
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "fsx:ListTagsForResource",
          "fsx:TagResource",
        ]
        Effect   = "Allow"
        Resource = "*"
      },
      {
        Action = [
          "elasticfilesystem:ListTagsForResource",
          "elasticfilesystem:TagResource",
        ]
        Effect   = "Allow"
        Resource = "*"
      },
      {
        Action = [
          "dynamodb:ListTagsOfResource",
          "dynamodb:TagResource",
        ]
        Effect   = "Allow"
        Resource = "*"
      },
      {
        Action = [
          "ec2:CreateTags",
          "ec2:DescribeTags",
        ]
        Effect   = "Allow"
        Resource = "*"
      },
      {
        Action = [
          "rds:AddTagsToResource",
          "rds:ListTagsForResource",
        ]
        Effect   = "Allow"
        Resource = "*"
      },
      {
        Action = [
          "storagegateway:AddTagsToResource",
          "storagegateway:ListTagsForResource",
        ]
        Effect   = "Allow"
        Resource = "*"
      },
      {
        Action = [
          "s3:GetBucketTagging",
          "s3:PutBucketTagging",
        ]
        Effect = "Allow",
        Resource = [
          "arn:aws:s3:::*",
        ]
      },
      {
        Action = [
          "s3:GetStorageLensConfigurationTagging",
          "s3:PutStorageLensConfigurationTagging",
        ]
        Effect = "Allow",
        Resource = [
          "arn:aws:s3:*:${var.discovery_account_id}:storage-lens/*",
        ]
      },
      {
        Action = [
          "s3:GetJobTagging",
          "s3:PutJobTagging",
        ]
        Effect = "Allow",
        Resource = [
          "arn:aws:s3:*:${var.discovery_account_id}:job/*",
        ]
      },
      {
        Action = [
          "s3:GetObjectVersionTagging",
          "s3:ReplicateTags",
          "s3:PutObjectVersionTagging",
          "s3:GetObjectTagging",
          "s3:PutObjectTagging",
        ],
        Effect = "Allow"
        Resource = [
          "arn:aws:s3:::*/*",
        ]
      }
    ]
  })
}

resource "aws_iam_policy" "openraven-database-secret-access" {
  # ORVNDbSecretAccess
  name = "openraven-database-secret-access-${var.openraven_org_id}"
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Condition = {
          StringEqualsIgnoreCase = {
            "aws:ResourceTag/OpenRavenOrgId" = [
              var.openraven_org_id,
            ]
          }
        }
        Action = [
          "secretsmanager:GetSecretValue",
        ]
        Resource = [
          "*",
        ]
        Effect = "Allow"
      },
      {
        Condition = {
          StringEqualsIgnoreCase = {
            "aws:ResourceTag/OpenRavenOrgId" = [
              var.openraven_org_id
            ]
          },
          StringLike = {
            "kms:ViaService" = [
              "secretsmanager.*.amazonaws.com",
            ]
          }
        }
        Action = [
          "kms:Decrypt",
        ]
        Resource = [
          "*",
        ],
        Effect = "Allow"
      },
      {
        Condition = {
          StringLike = {
            "aws:ResourceTag/OpenRavenAccessPermitted" = [
              "*",
            ]
          }
        }
        Action = [
          "secretsmanager:GetSecretValue",
        ]
        Resource = [
          "*",
        ]
        Effect = "Allow"
      },
      {
        Condition = {
          StringLike = {
            "aws:ResourceTag/OpenRavenAccessPermitted" = [
              "*",
            ]
          }
        }
        Action = [
          "kms:Decrypt",
        ]
        Resource = [
          "*",
        ],
        Effect = "Allow"
      },
    ]
  })
}

data "aws_iam_policy_document" "openraven-database-snapshot-creation" {
  # ORVNDbSnapshotCreation
  version = "2012-10-17"
  statement {
    effect = "Allow"
    actions = [
      "rds:CreateDBSnapshot",
      "rds:CreateDBClusterSnapshot",
      "rds:RestoreDBClusterFromSnapshot",
      "rds:RestoreDBInstanceFromDBSnapshot",

      "rds:CreateDBInstance",
      "rds:CreateDBCluster",

      "rds:CopyOptionGroup",
      "rds:DescribeOptionGroups",

      "ec2:CreateVpc",
      "ec2:CreateSubnet",
      "ec2:CreateRouteTable",
      "ec2:AssociateRouteTable",
      "ec2:CreateSecurityGroup",
      "ec2:ModifySecurityGroupRules",
      "ec2:AuthorizeSecurityGroupIngress",
      "ec2:CreateNetworkAcl",
      "rds:CreateDBSubnetGroup",
      "rds:ModifyDBSubnetGroup",
    ]
    resources = ["*"]
  }
}

data "aws_iam_policy_document" "openraven-database-snapshot-delete" {
  # ORVNDbSnapshotDelete
  version = "2012-10-17"
  statement {
    actions = [
      "rds:DeleteDBCluster",
      "rds:DeleteDBClusterSnapshot",
      "rds:ModifyDBCluster",

      "rds:DeleteDBInstance",
      "rds:DeleteDBSnapshot",
      "rds:ModifyDBInstance",

      "rds:DeleteOptionGroup",
      "rds:ModifyOptionGroup",

      "ec2:DeleteVpc",
      "ec2:DeleteSubnet",
      "ec2:DeleteRouteTable",
      "ec2:DeleteSecurityGroup",
      "ec2:DeleteNetworkAcl",
      "rds:DeleteDBSubnetGroup",
    ]
    resources = ["*"]
    condition {
      test     = "StringEquals"
      variable = "aws:ResourceTag/OPENRAVEN_DATA_CLASSIFICATION"
      values   = [var.openraven_org_id]
    }
  }
}

data "aws_iam_policy_document" "openraven-database-snapshot-encryption" {
  # ORVNDbSnapshotEncryption
  version = "2012-10-17"
  statement {
    actions = [
      "kms:CreateGrant",
      "kms:Decrypt",
      "kms:DescribeKey",
      "kms:Encrypt",
      "kms:GenerateDataKey*",
      "kms:ListGrants",
      "kms:ReEncrypt*",
    ]
    effect    = "Allow"
    resources = ["*"]
  }
}

resource "aws_iam_policy" "openraven-database-snapshot-creation" {
  description = "Policy granting the ability to create an RDS snapshots"
  count       = var.enable_rds_snapshot_scanning ? 1 : 0
  name        = "openraven-database-snapshot-creation-${var.openraven_org_id}"
  policy      = data.aws_iam_policy_document.openraven-database-snapshot-creation.json
}

resource "aws_iam_policy" "openraven-database-snapshot-deletion" {
  description = "Policy granting the ability to create an RDS snapshots"
  count       = var.enable_rds_snapshot_scanning ? 1 : 0
  name        = "openraven-database-snapshot-deletion-${var.openraven_org_id}"
  policy      = data.aws_iam_policy_document.openraven-database-snapshot-delete.json
}

resource "aws_iam_policy" "openraven-database-snapshot-encryption" {
  # ORVNDbSnapshotEncryption
  description = "Policy granting the ability to manage encryption around the snapshot"
  count       = var.enable_rds_snapshot_scanning ? 1 : 0
  name        = "openraven-database-snapshot-encryption-${var.openraven_org_id}"
  policy      = data.aws_iam_policy_document.openraven-database-snapshot-encryption.json
}

data "aws_iam_policy_document" "scanning-via-ec2" {
  statement {
    actions = [
      "ec2:DescribeInstances",
      "ec2:RunInstances",
      "ec2:CreateTags",
      "ec2:CreateVpc",
      "ec2:CreateSubnet",
      "ec2:CreateRouteTable",
      "ec2:CreateSecurityGroup",
      "ec2:ModifySecurityGroupRules",
      "ec2:AuthorizeSecurityGroupIngress",
      "ec2:CreateNetworkAcl",
      "ec2:CreateRoute",
      "ec2:CreateInternetGateway",
      "ssm:CancelCommand",
      "ssm:GetCommandInvocation",
      "ssm:GetConnectionStatus",
      "ssm:GetDocument",
      "ssm:DescribeSessions",
      "ssm:ListCommand*",
      "ssm:ResumeSession",
      "ssm:StartSession",
      "ssm:TerminateSession"
    ]

    effect = "Allow"
    resources = ["*"]
  }

  statement {
    actions = [
      "ec2:RebootInstances",
      "ec2:TerminateInstances",
      "ec2:ModifySubnetAttribute",
      "ec2:AssociateRouteTable",
      "ec2:AttachInternetGateway",
      "ec2:DeleteVpc",
      "ec2:DeleteSubnet",
      "ec2:DeleteRouteTable",
      "ec2:DeleteSecurityGroup",
      "ec2:DeleteNetworkAcl",
      "ec2:DeleteInternetGateway",
      "ec2:DetachInternetGateway",
      "ec2:DisassociateRouteTable"
    ]

    effect = "Allow"
    resources = ["*"]

    condition {
      test     = "StringEquals"
      variable = "aws:ResourceTag/OPENRAVEN_DATA_CLASSIFICATION"
      values   = [var.openraven_org_id]
    }
  }

  statement {
    actions = ["iam:PassRole"]
    effect  = "Allow"
    resources = [
      aws_iam_role.openraven-discovery-role.arn,
    ]
  }
}

resource "aws_iam_policy" "openraven-scanning-via-ec2" {
  description = "Policy granting the ability to scan via EC2"
  name        = "openraven-scanning-via-ec2-${var.openraven_org_id}"
  policy      = data.aws_iam_policy_document.scanning-via-ec2.json
}

resource "aws_iam_instance_profile" "ORVNDiscoveryRoleInstanceProfile" {
  name = "openraven-instance-profile-${var.openraven_org_id}"
  role = aws_iam_role.openraven-discovery-role.name
}

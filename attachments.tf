resource "aws_iam_role_policy_attachment" "openraven-attach-lambda-create" {
  role       = aws_iam_role.openraven-discovery-role.name
  policy_arn = aws_iam_policy.openraven-lambda-setup.arn
}

resource "aws_iam_role_policy_attachment" "openraven-attach-read-only" {
  role       = aws_iam_role.openraven-discovery-role.name
  policy_arn = "arn:aws:iam::aws:policy/ReadOnlyAccess"
}

resource "aws_iam_role_policy_attachment" "openraven-attach-aws-lambda-exec" {
  role       = aws_iam_role.openraven-discovery-role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaVPCAccessExecutionRole"
}

resource "aws_iam_role_policy_attachment" "openraven-attach-backup-enablement" {
  role       = aws_iam_role.openraven-discovery-role.name
  policy_arn = aws_iam_policy.openraven-backup-enablement.arn
}

resource "aws_iam_role_policy_attachment" "openraven-attach-database-access" {
  role       = aws_iam_role.openraven-discovery-role.name
  policy_arn = aws_iam_policy.openraven-database-secret-access.arn
}

resource "aws_iam_role_policy_attachment" "openraven-database-snapshot-creation" {
  count      = var.enable_rds_snapshot_scanning ? 1 : 0
  policy_arn = aws_iam_policy.openraven-database-snapshot-creation[0].arn
  role       = aws_iam_role.openraven-discovery-role.name
}

resource "aws_iam_role_policy_attachment" "openraven-database-snapshot-deletion" {
  count      = var.enable_rds_snapshot_scanning ? 1 : 0
  policy_arn = aws_iam_policy.openraven-database-snapshot-deletion[0].arn
  role       = aws_iam_role.openraven-discovery-role.name
}

resource "aws_iam_role_policy_attachment" "openraven-database-snapshot-encryption" {
  count      = var.enable_rds_snapshot_scanning ? 1 : 0
  policy_arn = aws_iam_policy.openraven-database-snapshot-encryption[0].arn
  role       = aws_iam_role.openraven-discovery-role.name
}

resource "aws_iam_role_policy_attachment" "openraven-scanning-via-ec2" {
  policy_arn = aws_iam_policy.openraven-scanning-via-ec2.arn
  role       = aws_iam_role.openraven-discovery-role.name
}
